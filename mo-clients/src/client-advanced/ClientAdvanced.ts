import { ClientEnum } from '@mo/interface/src/Enum.js'
import ClientBasic from '@/client-basic/ClientBasic.js'

export default class ClientAdvanced extends ClientBasic {
  constructor() {
    super()
    this.clientType = ClientEnum.Advanced
  }
}
