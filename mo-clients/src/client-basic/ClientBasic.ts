import { ClientEnum } from '@mo/interface/src/Enum.js'
import { IsClient } from '@mo/interface/src/Interface.js'

export default class ClientBasic implements IsClient {
  clientType: ClientEnum

  constructor() {
    this.clientType = ClientEnum.Basic
  }

  start(): Promise<void> {
    console.log('Starting Client: ' + this.clientType)
    return Promise.resolve()
  }
}
