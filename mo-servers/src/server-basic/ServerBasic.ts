import { ServerEnum } from '@mo/interface/src/Enum.js'
import { IsServer } from '@mo/interface/src/Interface.js'

export default class ServerBasic implements IsServer {
  serverType: ServerEnum

  constructor() {
    this.serverType = ServerEnum.Basic
  }

  start(): Promise<void> {
    console.log('Starting server: ' + this.serverType)
    return Promise.resolve()
  }
}
