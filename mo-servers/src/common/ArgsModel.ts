import { IsArg } from '@mo/interface/src/Interface.js'

export default class ArgsModel implements IsArg {
  arg?: any

  constructor(arg?: any) {
    this.arg = arg
  }
}
