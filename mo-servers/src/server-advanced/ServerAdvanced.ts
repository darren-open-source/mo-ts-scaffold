import ServerBasic from '@/server-basic/ServerBasic.js'
import { ServerEnum } from '@mo/interface/src/Enum.js'

export default class ServerAdvanced extends ServerBasic {
  constructor() {
    super()
    this.serverType = ServerEnum.Advanced
  }
}
