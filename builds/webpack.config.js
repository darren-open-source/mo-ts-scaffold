const path = require('path')
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin').TsconfigPathsPlugin

const serverPackages = '../packages/@mo/mo-servers/src'
const clientPackages = '../packages/@mo/mo-clients/src'

module.exports = [serverConfig, clientConfig]

function serverConfig() {
  return {
    name: 'server',
    mode: 'production',
    entry: {
      serverAdvanced: path.join(serverPackages, '/server-advanced/'),
      serverBasic: path.join(serverPackages, '/server-basic/')
    },
    output: {
      path: path.join(__dirname, 'servers'),
      filename: '[name].js'
    },
    resolve: {
      plugins: [
        new TsconfigPathsPlugin({
          configFile: '../packages/@mo/mo-servers/tsconfig.json'
          // configFile: '../mo-servers/tsconfig.json'
        })
      ]
    }
  }
}

function clientConfig() {
  return {
    name: 'client',
    mode: 'production',
    entry: {
      clientAdvanced: path.join(clientPackages, '/client-advanced/'),
      clientBasic: path.join(clientPackages, '/client-basic/')
    },
    output: {
      path: path.join(__dirname, 'clients'),
      filename: '[name].js'
    },
    resolve: {
      plugins: [
        new TsconfigPathsPlugin({
          configFile: '../packages/@mo/mo-clients/tsconfig.json'
        })
      ]
    }
  }
}
