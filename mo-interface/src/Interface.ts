import { ClientEnum, ServerEnum } from '@/Enum.js'

export interface IsClient {
  clientType: ClientEnum

  start(): Promise<void>
}

export interface IsServer {
  serverType: ServerEnum

  start(): Promise<void>
}

export interface IsArg {
  arg?: any | undefined
}
