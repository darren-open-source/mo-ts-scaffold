# Background

This repo simulates a small and relatively modular mono-repo with abstractions for Clients, Servers, Interfaces, and a Website. The main objective of each folder is as follows:  

**mo-interface**  
A common project shared between `mo-servers` and `mo-clients` that contains interfaces and enums to structure the implementations of the reference projects.  

**mo-clients**
A common project shared with `mo-servers` which uses the interfaces in `mo-interfaces` to create new clients that are able to connect to `mo-servers`

**mo-servers**  
A standalone project which uses the interfaces in `mo-interfaces` to create new servers that are able to talk to each other. Once service may use a client from `mo-clients` to connect to another server.

**mo-website**  
A not-as-yet-used website to create an easy UI for a user to control a client and interact with a server.

**packages**  
The `.js.` and `.d.ts` output files of all the modules

# Objectives

1)  ✅  Use VSCode to be able to debug into any of the modules using only one `launch.json` file  
2)  ❌  Automatically import a module from another module without having to first reference a file in that module  
3)  ✅  Build TS into JS within the `packages` directly, and correctly execute the code using `node`
4)  ✅  Derive all `tsconfig.json` from a base config
5)  ✅  Use `paths` instead of relative path imports for cleaner code { ❗️  This has forced me to use .js extensions }  
6)  ❓  `interface` and `clients` need to be built as standalone projects to upload to NPM as a package
7)  ✅  Executing `ts-node` on any ts file should work with no issue
8)  ✅  Each module in the packages folder should run without needing a local reference to other modules.
9)  ✅  Full refactor/rename support
10) ⚠️  **THIS WILL NEVER HAPPEN** Remove the need for .js extensions in imports  

# Entrypoint
As `mo-services` acts as the main glue for all the modules, this should be the entrypoint to attempt the objectives.

# Commands  
From the Entrypoint the notable commands are:  

`npm start` to execute `tsc-node` on the main file  
`npm run build` to execute `tsc --build` and compile all referenced modules  
`npm run node` to execute `node ../XX/XX` to try and run the resulting `main.js` file on a plain node instance


### References
package.json for Node16 (using commonjs): [link](https://stackoverflow.com/questions/67371787/what-typescript-configuration-produces-output-closest-to-node-js-16-capabilities/67371788#67371788)  

webpack multiple outputs: [link](https://stackoverflow.com/questions/35903246/how-to-create-multiple-output-paths-in-webpack-config)

force auto js on import [link](https://stackoverflow.com/questions/64270877/force-the-auto-import-to-use-js-extension-in-typescript-visual-studio-code-ed)

---

# Build Pipeline
- `mo-x` transpiled from TS (in /src/) to JS (out to ./packages/@mo/`mo-x`) `@package`
- `@package` compiled from JS (in /src) to JS (out to ./builds/) using `webpack`